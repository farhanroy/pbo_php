<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use SimpleFramework\Application;

$request = Request::createFromGlobals();
$kernel = new Application();

$response = $kernel->handle($request);
$response->send();
