<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$routes = new RouteCollection();

$routes->add('hello', new Route('/hello', [
    '_controller' => 'SimpleFramework\Controller\HelloController::hello',
]));

$routes->add('greeting', new Route('/greeting/{nama}', [
    'nama' => 'Farhan',
    '_controller' => 'SimpleFramework\Controller\HelloController::greet',
]));
