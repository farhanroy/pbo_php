<?php
class MagicMethod
{
    private function foo(string $name)
    {
        echo $name;
    }
    public function __call($name, $arguments)
    {
        if ('foo' === $name) {
            $this->foo($arguments[0]);
        } else {
            throw new Error(sprintf('Undefined method %s on
class %s', $name, __CLASS__));
        }
    }
}
$magic = new MagicMethod();
$magic->foo('Roy');
echo PHP_EOL;
