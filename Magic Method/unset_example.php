<?php
class MagicMethod
{
    private $data = [
        'name' => 'M. Roy Farchan',
        'address' => 'Pasuruan',
    ];
    public function __unset($property)
    {
        if (isset($this->data[$property])) {
            unset($this->data[$property]);
        }
    }
}
$magic = new MagicMethod();
var_dump($magic);
unset($magic->address);
var_dump($magic);
