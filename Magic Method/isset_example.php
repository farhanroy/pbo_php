<?php
class MagicMethod
{
    private $name;
    public function __isset($property)
    {
        if ('name' === $property) {
            return true;
        }
    }
}
$magic = new MagicMethod();
var_dump(isset($magic->name));
