<?php
class Lingkaran
{
    // konstanta harus ada keyword const
    public const PI = 3.14;

    public function luas($jari)
    {
        /* untuk mengakses konstanta didalam class 
        kita menggunakan keyword self dan diluar 
        class kita menggunakan nama class */
        echo self::PI * $jari * $jari;
    }
}

$lingkaran = new Lingkaran();

// Memanggil const variable dari kelas
echo Lingkaran::PI;
echo PHP_EOL;
$lingkaran->luas(7);
echo PHP_EOL;
