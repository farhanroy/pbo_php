<?php
class Lingkaran
{
    const PI = 3.14;
    public function luas($jari)
    {
        return self::PI * $jari * $jari;
    }
}
$lingkaran = new Lingkaran();
// hitung lingkaran dengan jari - jari 7
echo $lingkaran->luas(7);
echo PHP_EOL;
