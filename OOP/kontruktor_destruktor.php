<?php
//
class Connection
{
    /* Untuk membuat constructor kita harus membuat method dengan
    nama __construct() */
    public function __construct()
    {
        echo 'Lakukan koneksi database';
    }

    /* Untuk membuat destructor kita harus membuat method dengan
    nama __destruct(). fungsi ini dijalankan terakhir ketika 
    semua fungsi sudah dijalankan */
    public function __destruct()
    {
        echo 'Object dihapus dari memory';
    }
}

// INIT
$connection = new Connection();
unset($connection);
