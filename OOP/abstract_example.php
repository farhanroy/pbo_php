<?php
abstract class Hewan
{
    abstract public function setJenis($jenis);
    abstract public function getJenis();
}

class Kambing extends Hewan
{
    private $jenis;
    public function setJenis($jenis)
    {
        $this->jenis = $jenis;
    }
    public function getJenis()
    {
        return $this->jenis;
    }
}

$kambing = new Kambing();

$kambing->setJenis('Herbivora');
echo $kambing->getJenis();

echo PHP_EOL;
