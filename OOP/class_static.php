<?php
class Singa
{
    public static $KAKI = 4;

    // Pemanggilan static dengan menggunakan nama kelas itu sendiri
    public function kaki1()
    {
        echo Singa::$KAKI;
    }

    // Pemanggilan static dengan menggunakan keyword self
    public function kaki2()
    {
        echo self::$KAKI;
    }

    // Pemanggilan static dengan menggunakan keyword static
    public function kaki3()
    {
        echo static::$KAKI;
    }
}

$singa = new Singa();
echo $singa->kaki1();
echo PHP_EOL;
