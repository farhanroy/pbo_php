<?php
//
class Mobil {
    public $roda;

    /*
    *Sebuah property atau method yang diberikan visibilitas private
    *maka property atau method tersebut hanya dapat diakses dari
    *lingkup class dimana property atau method tersebut didefinisikan
    */
    private function jalan() {
        echo 'Jalankan mobil';
    }

    /* Sebuah property atau method yang diberikan visibilitas protected
    maka property atau method tersebut dapat diakses dari lingkup
    class dimana property atau method tersebut didefinisikan serta
    turunan dari class tersebut. */
    protected function jalan() {
        echo 'Jalankan mobil';
    }

    /* Sebuah property atau method yang diberikan visibilitas public maka
    property atau method tersebut dapat diakses baik dari lingkup
    class maupun objec */
    public function jumlahRoda() {
        echo 'Jalankan mobil';
    }
}

// Inctance class 
$avanza = new Mobil();

// call jalan() function
$avanza->jalan();

echo PHP_EOL;