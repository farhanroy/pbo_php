<?php
interface Hewan
{
    public function setJenis($jenis);
    public function getJenis();
}

class Kambing implements Hewan
{
    private $jenis;
    public function setJenis($jenis)
    {
        $this->jenis = $jenis;
    }
    public function getJenis()
    {
        return $this->jenis;
    }
}

$kambing = new Kambing();

$kambing->setJenis('Herbivora');
echo $kambing->getJenis();

echo PHP_EOL;
